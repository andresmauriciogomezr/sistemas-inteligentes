import pandas as pd
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense, Dropout
import matplotlib.pyplot as plt
import keras as keras

keras.utils.set_random_seed(1)

#Importa el archivo de datos
df = pd.read_csv('/Users/andresmauriciogomezr/Documents/Extraccioncaracteristicas/pima-indians-diabetes.data.csv')

# Convierte la información del archivo en una matriz de vectores
dataset = df.values

# Extra las columnas con la información de las entradas
X = dataset[:,0:8]

# Extra la columna de resultado
Y = dataset[:,8]

# Convierte las entradas para que tengan un valor entre 0 y 1
min_max_scaler = preprocessing.MinMaxScaler()
X_scale = min_max_scaler.fit_transform(X)

# Separa la información con los valores de entrada para entrenamiento y validación y pruebas
X_train, X_val_and_test, Y_train, Y_val_and_test = train_test_split(X_scale, Y, test_size=0.3)


X_train, X_val_and_test = X_scale[:536], X_scale[536:]
Y_train, Y_val_and_test = Y[:536], Y[536:]


# Separa la información de validación y pruebas en dos: validación y pruebas
#X_val, X_test, Y_val, Y_test = train_test_split(X_val_and_test, Y_val_and_test, test_size=0.5)
X_val, X_test = X_val_and_test[:115], X_val_and_test[115:]
Y_val, Y_test = Y_val_and_test[:115], Y_val_and_test[115:]

# Se define la arquitectura de la red neuronal, capa de entrada 8 neuronas, capa oculata 32 neuronas, capa oculta 32 neuronas, capa de salida una neurona
#model = Sequential([
    #Dense(32, activation='relu', input_shape=(8,)),
    #Dense(32, activation='relu'),
    #Dense(1, activation='sigmoid'),
#])

#model = Sequential([
    #Dense(30, activation='relu', input_shape=(8,)),
    #Dense(30, activation='relu'),
    #Dense(1, activation='sigmoid'),
#])
def entrenar():

    # Se define el modelo
    model = Sequential([
        Dense(12, activation='relu', input_shape=(8,)),
        Dense(16, activation='relu'),
        Dense(16, activation='relu'),
        Dense(14, activation='relu'),
        Dense(1, activation='sigmoid'),
    ])

    # Se compila con los hiperparametros 
    model.compile(optimizer='sgd', 
                  loss='binary_crossentropy',
                  metrics=['accuracy', keras.metrics.SpecificityAtSensitivity(0.5), keras.metrics.Precision()])

    # Entrena el modelo, y guarda el historico de movimientos
    hist = model.fit(X_train, Y_train,
              batch_size=32, epochs=200,
              validation_data=(X_test, Y_test))

    # Se realiza validación con datos desconocidos
    resultado = model.evaluate(X_val, Y_val)
    

    evaluacion = resultado[1]

    return evaluacion, hist

evaluacion, hist = entrenar()
keys = hist.history.keys()

#for key in keys:
    #print(key)
    #print(hist.history[key])
    #print()


count = 0
while evaluacion < 0.6:
    evaluacion, hist = entrenar()
    count = count + 1
    print("**********************************")
    print(count)


plt.plot(hist.history['accuracy'])
plt.plot(hist.history['val_accuracy'])
plt.title('Curva de Accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Entrenamiento', 'Evaluación'], loc='lower right')
plt.show()


plt.plot(hist.history['loss'])
plt.plot(hist.history['val_loss'])
plt.title('Curva de perdida de error')
plt.ylabel('Error')
plt.xlabel('Epoch')
plt.legend(['Entrenamiento', 'Evaluación'], loc='upper right')
plt.show()
