from keras.models import Sequential
from keras.layers import Dense
import numpy as numpy
import pandas as pandas
from sklearn.preprocessing import scale
from sklearn.model_selection import train_test_split
from keras.utils import np_utils
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plot
from pprint import pprint
import inspect

# Carga el dataset
documento = pandas.read_csv("/Users/andresmauriciogomezr/Documents/Extraccioncaracteristicas/pima-indians-diabetes.data.csv", header=None)

# Extrae la información de entrada
eje_x = documento.drop(8, axis=1)

# Extrae el resultado
eje_y = documento[8]

# Hace una transformación de los datos para escalarlos
eje_x = scale(eje_x)

# Parte los datos para entrenamiento y pruebas
X_train, X_test, Y_train, Y_test = train_test_split(eje_x, eje_y, test_size=0.33, random_state=42)

Y_train = np_utils.to_categorical(Y_train)

# Crea el modelo
model = Sequential()
model.add(Dense(12, input_dim=8, activation='relu'))
model.add(Dense(8, activation='relu'))
model.add(Dense(2, activation='sigmoid'))

model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

# Entrena el modelo
hist = model.fit(X_train, Y_train, epochs=500, batch_size=10)

# Hace una evaluación
y_pred = model.predict(X_test)
y_pred = numpy.argmax(y_pred, axis=1)

plot.plot(hist.history['loss'])
plot.xlabel('epoch')
plot.xlabel('loss')
plot.show()
